<?php

require_once 'fpdf/fpdf.php';
require_once 'logica/Administrador.php';
require_once 'logica/Cliente.php';
require_once 'logica/Domiciliario.php';

require_once 'logica/Metodo_pago.php';
require_once 'logica/Categoria.php';
require_once "logica/Producto.php";

require_once "logica/Factura.php";
require_once "logica/Pedido.php";
require_once "logica/Direccion_cliente.php";

$id_factura = $_GET["factura"];
$factura = new Factura($id_factura);
$factura->consultar();
$id_cliente = $factura->getCliente()->getId();
$direccion = new Direccion_cliente("", "", $id_cliente);
$direccion->consultar();

$pdf = new FPDF("P", "mm", "LETTER");
$pdf->AddPage();
// Definiendo fondo antes de la celda
$pdf->SetFont("Courier", "BI", 18);
// posicion x ancho,y alto,titulo,sinmarco,saltolinea,centrado
$pdf->Cell(196, 10, "Piensa Veggie", 0, 1, "C");

$pdf->SetFont("Courier", "", 14);
$pdf->Cell(196, 5, "Tienda virtual", 0, 1, "C");
$pdf->Cell(196, 7, "www.piensa_veggie.com", 0, 1, "C");

$pdf->SetFont("Courier", "B", 12);
$pdf->Cell(196, 8, "Factura de venta No. ", 0, 0, "R");
$pdf->Cell(3, 8, $factura->getId(), 0, 1, "R");
$pdf->Cell(175, 3, utf8_decode("Fecha de generación:"), 0, 0, "R");
$pdf->Cell(26, 5, $factura->getFecha(), 0, 1, "R");
$pdf->Cell(25, 7, "Cliente: ", 0, 0, "");
$pdf->Cell(30, 7, utf8_decode($factura->getCliente()
    ->getNombre()), 0, 0, "");
$pdf->Cell(30, 7, utf8_decode($factura->getCliente()
    ->getApellido()), 0, 1, "");
$pdf->Cell(27, 5, utf8_decode("Dirección: "), 0, 0, "");
$pdf->Cell(26, 5, $direccion->getDescripcion(), 0, 1, "");
$pdf->Cell(196, 12, "Detalle de venta", 0, 1, "C");
$pdf->Cell(80, 8, "Producto", 1, 0, "C");
$pdf->Cell(38, 8, "Unidades", 1, 0, "C");
$pdf->Cell(38, 8, "Precio", 1, 0, "C");
$pdf->Cell(38, 8, "Total", 1, 1, "C");

$pdf->SetFont("Courier","",12);
$pedido = new Pedido("","","",$id_factura);
$lista_productos_pedido = $pedido->consultar_por_factura();

foreach($lista_productos_pedido as $producto_a){

    $pdf->Cell(80, 8, $producto_a->getProducto()
        ->getDescripcion(), 1, 0, "C");
    $pdf->Cell(38, 8, $producto_a->getCantidad(), 1, 0, "C");
    $pdf->Cell(38, 8, "$ " . $producto_a->getProducto()
        ->getPrecio(), 1, 0, "C");
    $pdf->Cell(38, 8,"$ ". $producto_a->getSubtotal(), 1, 1, "C");
}


$pdf->SetFont("Courier","B",14);
$pdf->Cell(196,10,"TOTAL",0,1,"C");


$pdf->SetFont("Courier", "B", 10);
$pdf->Cell(156,8,"Concepto",1,0,"R");
$pdf->Cell(38,8,"Valor",1,1,"C");

$pdf->Cell(156,8,"Subtotal (precio sin IVA)",1,0,"R");
$pdf->Cell(38,8,"$ ".  $pedido->getFactura()->getSubtotal(),1,1,"C");
$pdf->Cell(156,8,"IVA 19%",1,0,"R");
$pdf->Cell(38,8,"$ ". $pedido->getFactura()->getImpuesto() ,1,1,"C");
$pdf->Cell(156,8,"TOTAL",1,0,"R");
$pdf->Cell(38,8,"$ ". $pedido->getFactura()->getTotal() ,1,1,"C");


$pdf->Output();
$pdf->Close();

?>