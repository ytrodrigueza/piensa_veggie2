<?php

// necesito esta funcion cuando utilizo variables de sesion, o saldria que esta indefinida la variable, ya en el index existe la sesion en todas las pag del proyecto
session_start();

// porque lo necesito en varios lados el admi o sino lo "importaria" al hacer la instancia como en autenticar
// $administrador = new Administrador("", "", "", $correo, $clave,""); entonces lo dejo en el index por facilidad
// al igual que cliente y domiciliario, si no hiciera esto debería colocarlo en cada .php que lo instanciara
require_once 'logica/Administrador.php';
require_once 'logica/Cliente.php';
require_once 'logica/Domiciliario.php';

// necesarios para los servicios de cliente y admin, donde más se puede?????????????????????????????
require_once 'logica/Metodo_pago.php';
require_once 'logica/Categoria.php';
require_once "logica/Producto.php";

//necesarios para creacion de factura en una compra
require_once "logica/Factura.php";
require_once "logica/Pedido.php";
require_once "logica/Direccion_cliente.php";

// si la variable de sesion existe por metodo get(en la URL)y tiene un valor falso literal valor falso con "comillas"
if (isset($_GET["sesion"]) && $_GET["sesion"] == "false") {

    // borrar variable de sesion
    $_SESSION["id"] = "";
    $_SESSION["rol"] = "";
}

// metodo Get trae lista de variables
$pid = "";

// existe la variable pid ?
if (isset($_GET["pid"])) {
    $pid = base64_decode($_GET["pid"]);
}

// en pag sin sesion no requiero colocar la de productos porque si no tengo sesion la incluye "presentacion/productos.php"

$paginas_sin_sesion = array(
    "presentacion/ingresar.php",
    "presentacion/registrar.php",
    "presentacion/autenticar.php",
    "presentacion/producto/listaproductos.php",
    "presentacion/producto/compra.php"
);

?>


<!doctype html>
<html lang="es">
<head>
<!-- utf-8  para las tíldes en español -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Bootstrap CSS -->
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css"
	rel="stylesheet">

<!-- JS , incluye popper (para los tooltips)-->
<script
	src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js">
	</script>

<!-- CSS -->
<link rel="stylesheet" href="css/estilos.css" />

<!--iconos font awesome-->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />

<!--JQUERY libreria de JS-->
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>



<!-- libreria para hacer gráficos-->

<script src="https://www.gstatic.com/charts/loader.js"></script>

<!-- letra -->
<link
	href="https://fonts.googleapis.com/css2?family=Fredericka+the+Great&display=swap"
	rel="stylesheet">
<!-- para graficos de google static, la libreria loader-->
<script src="https://www.gstatic.com/charts/loader.js"></script>


<!-- Titulo pestaña con imágen -->
<title>Piensa Veggie</title>
<link rel="icon" type="image/png" href="img/logo_pesta.png" />


<!-- para incluir un archivo con algo, en este caso es el encabezado del proyecto-->


<?php
if ($pid != "") {
    if (in_array($pid, $paginas_sin_sesion)) {
        include $pid;
    } else {
        if (isset($_SESSION["id"]) && $_SESSION["id"] != "") {
            include $pid;
        
        } else {
            include "presentacion/ingresar.php";
        }
    }
} else {
    include "presentacion/productos.php";
}

?>


</head>

<body> GÜENAS MUNDO!


</body>
<footer>

	<div class="container">
		<div class="row mt-3">
			<div class="row">
				<div class="col text-center text-muted">
					<i class="fas fa-skull "></i> Y &copy; <?php echo date("Y") ?>	
				</div>
			</div>
		</div>
	</div>


</footer>
</html>
