<?php

include 'presentacion/menu_domiciliario.php';

$domiciliario=new Domiciliario($_SESSION["id"]);
$domiciliario->consultar();
$rol=$_SESSION["rol"];
?>


<div class="container">
	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<h5 class="card-header"> <?php echo $domiciliario->getNombre()." ". $domiciliario->getApellido()?></h5>
				<div class="card-body"> 
					<?php echo "Correo: " . $domiciliario -> getCorreo(); ?>
				</div>
			</div>
		</div>
	</div>
</div>