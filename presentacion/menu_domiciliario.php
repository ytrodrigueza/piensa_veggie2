<?php
$domiciliario = new Domiciliario($_SESSION["id"]);
$domiciliario->consultar();
?>


<div>
	<nav class="navbar navbar-expand-lg  bg-dark">
		<div class="container-fluid">
			<a class="navbar-brand"
				href="index.php?pid=<?php echo base64_encode("presentacion/sesion_domiciliario.php") ?>"><img
				src="img/logo_pesta.png" alt="logo principal" width="50" /></a>
			<button class="navbar-toggler " type="button"
				data-bs-toggle="collapse" data-bs-target="#navbarNavDarkDropdown"
				aria-controls="navbarNavDarkDropdown" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>

			</button>
			<div class="collapse navbar-collapse" id="navbarNavDarkDropdown">
				<ul class="navbar-nav">

					<li class="nav-item dropdown"><a class="nav-link dropdown-toggle"
						href="#" id="navbarDropdown" role="button"
						data-bs-toggle="dropdown" aria-expanded="false"> Mi perfil</a>
						<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
							<li><a class="dropdown-item"
								href="index.php?pid=<?php echo base64_encode("presentacion/sesion_cliente.php") ?>">Consultar</a></li>
						</ul></li>

					<li class="nav-item" id="productos"><a class="nav-link" href="index.php?pid=<?php echo base64_encode("presentacion/productos.php") ?>">Productos</a></li>


					<li class="nav-item dropdown"><a class="nav-link dropdown-toggle"
						href="#" id="navbarDropdown" role="button"
						data-bs-toggle="dropdown" aria-expanded="false">Entregas</a>
						<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
							<li><a class="dropdown-item" href="#">Historico de entregas</a></li>
							<li><a class="dropdown-item" href="#">Entregas pendientes</a></li>
						</ul></li>
						
					<li class="nav-item dropdown"><a class="nav-link dropdown-toggle"
						href="#" id="navbarDropdown" role="button"
						data-bs-toggle="dropdown" aria-expanded="false">Cliente</a>
						<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
							<li><a class="dropdown-item"
								href="#">Consultar</a></li>
						</ul></li>
				
				</ul>

				<ul class="navbar-nav">

					<li class="nav-item" ><a class="nav-link" href="#">Bienvenido Domiciliario: <?php echo $domiciliario -> getNombre() . " " . $domiciliario -> getApellido() ?></a></li>

					<li class="nav-item"><a class="nav-link" href="index.php?sesion=false">Cerrar Sesion</a></li>
				</ul>

			</div>
		</div>

	</nav>
</div>