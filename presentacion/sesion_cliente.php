<?php

include 'presentacion/menu_cliente.php';

$cliente=new Cliente($_SESSION["id"]);
$cliente->consultar();
$rol=$_SESSION["rol"];

?>


<div class="container">
	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<h5 class="card-header"> <?php echo $cliente->getNombre()." ". $cliente->getApellido()?></h5>
				<div class="card-body"> 
					<?php echo "Correo: " . $cliente -> getCorreo(); ?>
				</div>
			</div>
		</div>
	</div>
</div>