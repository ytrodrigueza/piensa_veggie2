<?php

if(isset($_SESSION["id"]) && $_SESSION["id"] !=""){
   
    if($_SESSION["rol"]=="cliente"){
        include 'presentacion/sesion_cliente.php';
    }
    
    else if($_SESSION["rol"]=="administrador"){
        include 'presentacion/sesion_admi.php';
    } 
    
    else if($_SESSION["rol"]=="domiciliario"){
        include 'presentacion/sesion_domiciliario.php';
    } 
    
}else{
    include "presentacion/encabezado.php";}
    
        
?>


<div>
	<br />
</div>
<div class="container">
	<div class="row">
		<div class="col-sm">
			<div class="card" style="width: 18rem;">
				<img src="img/verdura.png" class="card-img-top" alt="verduras">
				<div class="card-body">
					<p class="card-text">Aca encontrara el listado de las Verduras.</p>
					<a href="#" class="btn btn-primary">Verdura</a>
				</div>
			</div>
		</div>

		<div class="col-sm">
			<div class="card" style="width: 18rem;">
				<img src="img/fruta.png" class="card-img-top" alt="frutas">
				<div class="card-body">
					<p class="card-text">Aca encontrara el listado de las Frutas.</p>
					<a href="#" class="btn btn-primary">Fruta</a>
				</div>
			</div>
		</div>

		<div class="col-sm">
			<div class="card" style="width: 18rem;">
				<img src="img/frijol.png" class="card-img-top" alt="Granos">
				<div class="card-body">
					<p class="card-text">Aca encontrara el listado de los Granos.</p>
					<a href="#" class="btn btn-primary">Granos</a>
				</div>
			</div>
		</div>
	</div>
	
	<br />
	
	<div class="row">

		<div class="col-sm">
			<div class="card" style="width: 18rem;">
				<img src="img/semillas.png" class="card-img-top" alt="Semillas">
				<div class="card-body">
					<p class="card-text">Aca encontrará el listado de las semillas.</p>
					<a href="#" class="btn btn-primary">Semillas</a>
				</div>
			</div>
		</div>

		<div class="col-sm">
			<div class="card" style="width: 18rem;">
				<img src="img/suplemento.png" class="card-img-top" alt="Suplementos">
				<div class="card-body">
					<p class="card-text">Aca encontrará el listado de los suplementos.</p>
					<a href="#" class="btn btn-primary">Suplementos</a>
				</div>
			</div>
		</div>

		<div class="col-sm">
			<div class="card" style="width: 18rem;">
				<img src="img/snack_veggi.png" class="card-img-top" alt="Snacks">
				<div class="card-body">
					<p class="card-text">Aca encontrará el listado de los snacks.</p>
					<a href="#" class="btn btn-primary">Snacks</a>
				</div>
			</div>
		</div>



	</div>

	<br />

	<div class="row">


		<div class="col-sm">
			<div class="card" style="width: 18rem;">
				<img src="img/fruto_seco.png" class="card-img-top"
					alt="Frutos_secos">
				<div class="card-body">
					<p class="card-text">Aca encontrara el listado de los frutos secos.</p>
					<a href="#" class="btn btn-primary">Frutos Secos</a>
				</div>
			</div>
		</div>

		<div class="col-sm">
			<div class="card" style="width: 18rem;">
				<img src="img/tofu.png" class="card-img-top" alt="Lácteos">
				<div class="card-body">
					<p class="card-text">Aca encontrará alternativas con lácteos
						vegetales</p>
					<a href="#" class="btn btn-primary">Lácteos</a>
				</div>
			</div>
		</div>

		<div class="col-sm">
			<div class="card" style="width: 18rem;">
				<img src="img/cereal.png" class="card-img-top" alt="Cereal">
				<div class="card-body">
					<p class="card-text">Aca encontrara el listado de los cereales.</p>
					<a href="#" class="btn btn-primary">Cereal</a>
				</div>
			</div>
		</div>



	</div>

<br />
		<div class="row ">
		
			<div class="col-sm"></div>
		
			<div class="col-sm">
			<div class="card" style="width: 18rem;">
				<img src="img/Logo.png" class="card-img-top" alt="mercado">
				<div class="card-body">
					<p class="card-text">Aca encontrará todos nuestros productos</p>
					<a href="index.php?pid=<?php echo base64_encode("presentacion/producto/listaproductos.php")?>" class="btn btn-primary">todos</a>
				</div>
			</div>
			</div>
			
			<div class="col-sm"></div>
			
		</div>
	
<div>

LISTA DE PRODUCTOS CON AJAX :D




</div>	
	
	
	
</div>

