<?php
// DE UN SOLO PRODUCTO
if (isset($_SESSION["id"]) && $_SESSION["id"] != "") {
    
     if ($_SESSION["rol"] == "administrador") {
        include 'presentacion/sesion_admi.php';
    } 
} else {
    include "presentacion/encabezado.php";
}

$producto=new Producto();
$productos_por_categoria = $producto -> productos_por_categoria();
$productos_por_fecha= $producto->productos_por_fecha();
?>


<div class="container">
	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<h5 class="card-header">Estadisticas</h5>
				<div class="card-body ">
					<div id="productos_por_categoria" style="width: 1000px; height: 300px;"></div>
					<div id="productos_por_fecha" style="width: 1000px; height: 300px;"></div>
					
				</div>
			</div>
		</div>
	</div>
</div>



<script type="text/javascript">
google.charts.load("current", {packages:['corechart']});
google.charts.setOnLoadCallback(drawChart); 


function drawChart() {
    var data_categoria = google.visualization.arrayToDataTable([
        ["Categoria", "Cantidad"],
        <?php 
        foreach ($productos_por_categoria as $pc){
            echo "['" . $pc[0] . "', " . $pc[1] . "],\n";        
        }        
        ?>
    ]);
    
    var view_categoria = new google.visualization.DataView(data_categoria);
    
    var options_categoria = {
        title: "Productos por Categoria",
        bar: {groupWidth: "95%"},
        legend: { position: "none" },
    };
    
    var chart_categoria = new google.visualization.ColumnChart(document.getElementById("productos_por_categoria"));
    chart_categoria.draw(view_categoria, options_categoria);

//fin de primer grafico
    
    var data_fecha = google.visualization.arrayToDataTable([
        ["Fecha", "Cantidad"],
        <?php 
        foreach ($productos_por_fecha as $p){
            echo "['" . $p[0] . "', " . $p[1] . "],\n";        
        }        
        ?>
    ]);
    
    var view_fecha = new google.visualization.DataView(data_fecha);
    
    var options_fecha = {
        title: "Cantidad de ventas por fecha",
        bar: {groupWidth: "95%"},
        legend: { position: "right" },
        is3D: true,
    };
    var chart_fecha = new google.visualization.PieChart(document.getElementById("productos_por_fecha"));
    chart_fecha.draw(view_fecha, options_fecha);





}
</script>

