
<?php

if (isset($_SESSION["id"]) && $_SESSION["id"] != "") {

    if ($_SESSION["rol"] == "cliente") {
        include 'presentacion/sesion_cliente.php';
    } 
    else if ($_SESSION["rol"] == "administrador") {
        include 'presentacion/sesion_admi.php';
    } 
    else if ($_SESSION["rol"] == "domiciliario") {
        include 'presentacion/sesion_domiciliario.php';
    }
} else {
    include "presentacion/encabezado.php";
}

// $factura=new Factura("",date('y:m:d'));

  $id_producto = $_GET["producto"];
  $producto = new Producto($id_producto);
  $producto->consultar(); // me trae las caracteristicas del producto comprado
  $inventario_inicial = $producto->getInventario();





/*if(isset($_POST["comprar"])){
    
    $producto=new Producto($id_producto,"","",$_POST[""])
    
    
    $cliente = new Cliente("", $_POST["nombre"], $_POST["apellido"], $_POST["correo"], $_POST["clave"], $_POST["celular"], 1);
    $cliente->registrar();


}
   */ 
 

?>

<div class="container">
	<div class="row mt-3">
		<div class="col-4"></div>
		<div class="col-4">
			<div class="card">
				<h5 class="card-header">Compra</h5>
				<div class="card-body">
					
					<?php if (isset($_POST["comprar"])) { ?>
					<div class="alert alert-success alert-dismissible fade show"
						role="alert">
						Datos ingresados correctamente.
						<button type="button" class="btn-close" data-bs-dismiss="alert"
							aria-label="Close"></button>
					</div>
					<?php } ?>
					
					<form
						action="index.php?pid=<?php echo base64_encode("presentacion/producto/compra_finalizada.php")?>"
						method="post">
						<input type="hidden"  name="id_producto" value=<?php echo $id_producto ?>>
						<div class="mb-3">
							<label class="form-label">Nombre</label> <label
								class="form-label"><?php echo $producto->getDescripcion()?></label>
								
						</div>
						<div class="mb-3">
							<label class="form-label">Precio</label> <label
								class="form-label">$<?php echo $producto->getPrecio()?></label>
						</div>
						<div class="mb-3">
							<label class="form-label">Inventario</label> 
							<label class="form-label" id="inventario"><?php echo $inventario_inicial?></label>
							<input type="hidden"  name="inventario" id="inventario_restante" value="0"/>
						</div>


						<div class="mb-3">
							<label class="form-label">Categoria</label> <label
								class="form-label"><?php echo $producto->getCategoria()->getDescripcion()?></label>
						</div>

						<div class="mb-3">
							<label class="form-label">Cantidad a comprar</label> <input
								type="number" class="form-control" name="cantidad" id="unidades"
								min="1" max="<?php echo $inventario_inicial?>" required/>
								<input type="hidden"  name="cantidad" id="cantidad_comprada" value="0"/>
						</div>

						<div class="mb-3">
						<label class="form-label">Metodo de pago: </label>
								
								<select  id="metodo_pago">
								<?php 
								$metodo_pago = new Metodo_pago();
								$metodos_pago=$metodo_pago->consultar_todos();
								foreach ($metodos_pago as $metodo_a){
								    echo "<option value='" . $metodo_a -> getId() . "'>" . $metodo_a -> getDescripcion() . "</option>";
								}
								
								?>
									
								</select>
							<input type="hidden"  name="metodo_pago" id="metodo_pago_seleccionado" value="0"/>
						</div>
						
						<div class="mb-3">
							<label class="form-label">Total</label> <label class="form-label"
								id="resultados"></label>
								<input type="hidden"  name="subtotal" id="subtotal_pedido" value="0"/>
						</div>

						<div class="d-grid">
							<button type="submit" name="comprar" class="btn btn-primary" id="compra">Comprar</button>
							<input type="hidden"  name="fecha" value=<?php echo date('Y-m-d'); ?>>
							<input type="hidden"  name="hora" value=<?php echo date("H:i",(time()-(7*60*60)));	 ?>>
						
						</div>
						</br>
						<div class="d-grid">

							<a href="index.php?pid=<?php echo base64_encode("presentacion/producto/listaproductos.php")?>"><button
									type="button" class="btn btn-outline-secondary">Regresar</button></a>

						</div>
					</form>

				</div>
			</div>
		</div>
	</div>
</div>



<script>

let input = document.querySelector('#unidades');
let max= input.getAttribute('max');


$(document).ready(function() {


$( "#unidades" ).change(function() {
 var unidades = $( "#unidades" ).val(); 
 alert("Hizo click unidades :D " + unidades + " " + max);
 let precio_producto =<?php echo $producto->getPrecio()?>;
 var inventario_total=0;

if(unidades<0 || unidades><?php echo $inventario_inicial ?>){
 alert ("UNIDADES FUERA DE RANGO= "+ unidades );
  }else{
  	    inventario_total=max-unidades;
 		var total= precio_producto * unidades;

		$("#inventario").text(inventario_total);
		$("#resultados").text("$"+total);
  	    alert ("inventario nuevo= "+ inventario_total+ " unidades=" + unidades + "TOTAL =" + total );
  }
 
 var inp1= document.querySelector('#inventario_restante');
 inp1.setAttribute('value',inventario_total);
 var inp2 = document.querySelector('#cantidad_comprada');
 inp2.setAttribute('value',unidades);
 var inp3 = document.querySelector('#subtotal_pedido');
 inp3.setAttribute('value',total);
 
});

 $("#metodo_pago").change(function(){
    	var id_pago= $("#metodo_pago").val();
        alert("Hizo click en el metodo pago con valor: "+ id_pago);
        
       var inp4= document.querySelector('#metodo_pago_seleccionado'); 
       inp4.setAttribute('value',id_pago);
    });




});
</script>



