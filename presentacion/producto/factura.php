
<?php

if (isset($_SESSION["id"]) && $_SESSION["id"] != "") {
    
    if ($_SESSION["rol"] == "cliente") {
        include 'presentacion/sesion_cliente.php';
    } else if ($_SESSION["rol"] == "administrador") {
        include 'presentacion/sesion_admi.php';
    } else if ($_SESSION["rol"] == "domiciliario") {
        include 'presentacion/sesion_domiciliario.php';
    }
} else {
    include "presentacion/encabezado.php";
}


    $id_factura=$_GET["factura"];
    $factura= new Factura($id_factura);
    $factura->consultar();
    
    $id_cliente = $factura->getCliente()->getId();
    $direccion = new Direccion_cliente("", "", $id_cliente);
    $direccion->consultar();
    
    echo "ID_FACTURA =". $id_factura;
    
    ?>


<div class="container">
	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<h5 class="card-header text-center"> Piensa Veggie</h5><br>
				<h5 class="card-header text-center"> Tienda virtual </h6><br>
				<h5 class="card-header text-center"> www.piensa_veggie.com </h6><br>
				
				<div class="card-body">
				
				<div> 
				<h5>Factura de venta No. <?php echo $id_factura; ?></h5><br>
				<h5>Fecha de generación <?php echo $factura->getFecha(); ?></h5><br>
				<h5>Cliente:  <?php echo $factura->getCliente()->getNombre()."  ".$factura->getCliente()->getApellido(); ?></h5><br>
				<h5>Direccion:  <?php echo $direccion->getDescripcion()?></h5><br>
				 </div>
				
				
				<div class="text-center"> <h4><b> Detalle de venta </b> </h4>  </div>
								<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th>Producto </th>
								<th>Unidades compradas</th>
								<th>Precio c/u</th>
								<th>Total</th>
								</tr>
							
						
						</thead>
						<tbody>
							<?php
							$pedido=new Pedido("","","",$factura->getId());
							
							$lista_productos_pedido=$pedido->consultar_por_factura();
							
							foreach ($lista_productos_pedido as $producto_a) {
							    
							  
                                echo "<tr>";
                                echo "<td>" . $producto_a->getProducto()->getDescripcion() . "</td>
                                      <td >" . $producto_a->getCantidad() . "</td>
                                      <td>" ."$". $producto_a->getProducto()->getPrecio() . "</td>
                                      <td>" . "$".$producto_a->getSubtotal(). "</td>";
                                      
                                                                    
                                                                      
                                
                                
                                
                                      
                                echo "</tr>";
                                
                            } 
                            ?>
                            
                            
                            
                            
						</tbody>
					</table>
				
				
					<div class= "text-center"><h6><b>TOTAL</b> </h6> </div>
						
					<table class="table table-striped table-hover ">
						<thead>
							<tr>
								<th  style="text-align: right"  >Concepto</th>
								<th  style="text-align: right" >Valor</th>
							</tr>
							
						
						</thead>
						<tbody>
							
							
                            <?php //Realizar esas operaciones y se pueden enviar por post a  la app o se puede calcular en la app e invocar los metodos?>
                            <tr>
                            <td  style="text-align: right" >Subtotal (precio sin IVA) </td>
                           <td  style="text-align: right" > $<?php echo $pedido->getFactura()->getSubtotal() //total(suma de subtotales de arriba) - IVA ?>  </td>
                            </tr>
                            
                            <tr>
                            <td style="text-align: right" >IVA 19%</td>
                            <td style="text-align: right" > $<?php echo $pedido->getFactura()->getImpuesto() //total(suma de los subtotales de la tabla de arriba) * 0.19?></td>
                            </tr>
                            
                            <tr>
                            <td style="text-align: right" >TOTAL</td>
                            <td style="text-align: right" 	>$<?php echo $pedido->getFactura()->getTotal() //suma de subtotal e IVA?></td>
                            </tr>
                            
                           
                           
                            
                            
                             
                              
                             
                            
						</tbody>
					</table>
						

				</div>
			</div>
		</div>
	</div>
</div>
