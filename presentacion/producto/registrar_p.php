<?php
include 'presentacion/menu_admi.php';


if(isset($_POST["registrar_p"])){
    echo  $_POST["descripcion"];
    $producto= new Producto($_POST["id"], $_POST["descripcion"], $_POST["precio"], $_POST["inventario"] ,$_SESSION["id"], $_POST["categoria"]);
    $producto->registrar();
    
}
?>
<div class="container">
	<div class="row mt-3">
		<div class="col-4"></div>
		<div class="col-4">
			<div class="card">
				<h5 class="card-header">Registrar producto</h5>
				<div class="card-body">
					<?php if(isset($_POST["registrar_p"])) { ?>
					<div class="alert alert-success alert-dismissible fade show"
							role="alert">
							Datos registrados correctamente
							<button type="button" class="btn-close" data-bs-dismiss="alert"
								aria-label="Close"></button>
					</div>
					<?php } ?>				
					<form method="post" action="index.php?pid=<?php echo base64_encode("presentacion/producto/registrar_p.php")?>" >
						<div class="mb-3">							
							<label for="exampleInputEmail1" class="form-label">Id </label>
							<input type="number" class="form-control" name="id" required="required">							
						</div>
						<div class="mb-3">							
							<label for="exampleInputEmail1" class="form-label">Descripcion</label>
							<input type="text" class="form-control" name="descripcion" required="required">
						</div>
						<div class="mb-3">							
							<label for="exampleInputEmail1" class="form-label">Precio</label>
							<input type="number" class="form-control" name="precio" required="required">
						</div>
						<div class="mb-3">							
							<label for="exampleInputEmail1" class="form-label"> Inventario </label>
							<input type="number" class="form-control" name="inventario" required="required">
						</div>
						<div class="mb-3">	
							<label  class="form-label">Categoria</label>
    						<select class="form-select" name="categoria">
    							<?php 
    							$categoria = new Categoria();
    							$categorias = $categoria-> consultar_todo();
    							foreach ($categorias as $categoria_actual){
    							    echo "<option value='" . $categoria_actual -> getId() . "'>" . $categoria_actual -> getDescripcion() . "</option>";
    							}
    							?>
    						</select>
						</div>
						<button type="submit" class="btn btn-primary" name="registrar_p">Registrar</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>