<?php
//include "presentacion/menu_cliente.php";

if(isset($_SESSION["id"]) && $_SESSION["id"] !=""){
    
    if($_SESSION["rol"]=="cliente"){
        include 'presentacion/sesion_cliente.php';
    }
    
    else if($_SESSION["rol"]=="administrador"){
        include 'presentacion/sesion_admi.php';
    }
    
    else if($_SESSION["rol"]=="domiciliario"){
        include 'presentacion/sesion_domiciliario.php';
    }
    
}else{
    include "presentacion/encabezado.php";}



//Invalid argument supplied for foreach Sucede cuando el foreach no encuentra una lista con la que iterar
$producto = new Producto();
$productos = $producto->consultar_todo();

?>


<div class="container">
	<div class="row mt-3">
		<div class="col">
			<div class="card">
				<h5 class="card-header">Consultar productos</h5>
				<div class="card-body">
				
				
								<table class="table table-striped table-hover">
						<thead>
							<tr>
								<th>#</th>
								<th>Producto</th>
								<th>Precio</th>
								<th class="text-center">Cantidad disponible</th>
								<th>Administrador</th>
								<th>Categoria</th>
								<th>		</th>
							</tr>
							
						
						</thead>
						<tbody>
							<?php
							$i=1;
							foreach ($productos as $productos_a) {
                                echo "<tr>";
                                echo "<td>" .$i++ . "</td>
                                      <td>" . $productos_a->getDescripcion() . "</td>
                                      <td>" . $productos_a->getPrecio() . "</td>
                                      <td class='text-center'>" . $productos_a->getInventario() . "</td>
                                      <td>" . $productos_a->getAdministrador()->getNombre() . "</td>
                                      <td>" . $productos_a->getCategoria()->getDescripcion()  . "</td>
                                      <td> <a href=# > <i class='fas fa-shopping-cart'></i></a> </td>                                   
                                      <td> <a href='index.php?pid=".base64_encode("presentacion/producto/compra.php"). "&producto=".$productos_a->getId()."'> <i class='fas fa-shopping-basket'></i> </a> </td>" ;                                
                                
                                
                                
                                      
                                echo "</tr>";
                            }
                            ?>
                            
                            
                            
						</tbody>
					</table>
				
				
					
						
						

				</div>
			</div>
		</div>
	</div>
</div>
