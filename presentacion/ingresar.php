<?php
include "presentacion/encabezado.php";
$error=0;
?>

<div class="container">
	<div class="row mt-3">
		<div class="col-xs-12 col-lg-4 text-center"></div>
		<div class="col-xs-12 col-lg-4 text-center">
			<div class="card">
				<h5 class="card-header bg-primary text-white">Iniciar Sesión</h5>
				<div class="card-body">
					<form method="post" action="index.php?pid=<?php echo base64_encode("presentacion/autenticar.php") ?>">
						<div class="mb-3">							
							<input type="email" class="form-control" name="correo" placeholder="Correo" required>							
						</div>
						<div class="mb-3">							
							<input type="password" class="form-control" name="clave" placeholder="Clave" required>
						</div>
						<button type="submit" class="btn btn-primary">Ingresar</button>
					<?php if ($error == 1) { ?>
						<div class="alert alert-danger alert-dismissible fade show"
							role="alert">
							Correo o clave incorrectos
							<button type="button" class="btn-close" data-bs-dismiss="alert"
								aria-label="Close"></button>
						</div>
					<?php } ?>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>

