<?php
$administrador = new Administrador($_SESSION["id"]);
$administrador->consultar();
?>
<div>
	<nav class="navbar navbar-expand-lg  bg-dark">
		<div class="container-fluid">
			<a class="navbar-brand"
				href="index.php?pid=<?php echo base64_encode("presentacion/sesion_admi.php") ?>"><img
				src="img/logo_pesta.png" alt="logo principal" width="50" /></a>
			<button class="navbar-toggler " type="button"
				data-bs-toggle="collapse" data-bs-target="#navbarNavDarkDropdown"
				aria-controls="navbarNavDarkDropdown" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>

			</button>
			<div class="collapse navbar-collapse" id="navbarNavDarkDropdown">
				<ul class="navbar-nav">

					<li class="nav-item dropdown"><a class="nav-link dropdown-toggle"
						href="#" id="navbarDropdown" role="button"
						data-bs-toggle="dropdown" aria-expanded="false">Administrador</a>
						<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
							<li><a class="dropdown-item"
								href="index.php?pid=<?php echo base64_encode("presentacion/sesion_admi.php") ?>">Consultar</a></li>
						</ul></li>

					<li class="nav-item dropdown"><a class="nav-link dropdown-toggle"
						href="#" id="navbarDropdown" role="button"
						data-bs-toggle="dropdown" aria-expanded="false">Productos</a>
						<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
							<li><a class="dropdown-item"
								href="index.php?pid=<?php echo base64_encode("presentacion/producto/registrar_p.php") ?>">Registrar</a></li>
							<li><a class="dropdown-item"
								href="index.php?pid=<?php echo base64_encode("presentacion/productos.php") ?>">Consultar</a></li>
						</ul></li>

					<li class="nav-item dropdown"><a class="nav-link dropdown-toggle"
						href="#" id="navbarDropdown" role="button"
						data-bs-toggle="dropdown" aria-expanded="false">Categorias
							productos</a>
						<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
							<li><a class="dropdown-item" href="#">Registrar</a></li>
							<li><a class="dropdown-item" href="#">Consultar</a></li>
						</ul></li>

					<li class="nav-item dropdown"><a class="nav-link dropdown-toggle"
						href="#" id="navbarDropdown" role="button"
						data-bs-toggle="dropdown" aria-expanded="false">Metodos de pago</a>
						<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
							<li><a class="dropdown-item" href="#">Registrar</a></li>
							<li><a class="dropdown-item" href="#">Consultar</a></li>
						</ul></li>

					<li class="nav-item dropdown"><a class="nav-link dropdown-toggle"
						href="#" id="navbarDropdown" role="button"
						data-bs-toggle="dropdown" aria-expanded="false">Clientes</a>
						<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
							<li><a class="dropdown-item" href="#">Consultar</a></li>
						</ul></li>

					<li class="nav-item dropdown"><a class="nav-link dropdown-toggle"
						href="#" id="navbarDropdown" role="button"
						data-bs-toggle="dropdown" aria-expanded="false">Domiciliarios</a>
						<ul class="dropdown-menu" aria-labelledby="navbarDropdown">
							<li><a class="dropdown-item" href="#">Registrar</a></li>
							<li><a class="dropdown-item" href="#">Consultar</a></li>
						</ul></li>


					<li class="nav-item" id="productos"><a class="nav-link" href=#>Facturas</a></li>

					<li class="nav-item"><a class="nav-link"
						href="index.php?pid=<?php echo base64_encode("presentacion/producto/estadisticas.php")?>">Estadisticas</a></li>

				</ul>

				<ul class="navbar-nav">
					<li class="nav-item"><a class="nav-link"
						href="index.php?sesion=false">Administrador: <?php echo $administrador -> getNombre() . " " . $administrador -> getApellido() ?></a></li>
					<li class="nav-item"><a class="nav-link"
						href="index.php?sesion=false">Cerrar Sesion</a></li>
				</ul>


<!-- para las notificaciones-->
<?php $count=1;?>
				<div class="demo-content">
					<div id="notification-header">
						<div style="position: relative">
							<button id="notification-icon" name="button"
								onclick="myFunction()" class="dropbtn">
								<span id="notification-count"><?php if($count>0) { echo $count; } ?></span><img
									src="img/notificaciones.png" />
							</button>
							<div id="notification-latest"></div>
						</div>
					</div>
				</div>


			</div>
		</div>

	</nav>
</div>