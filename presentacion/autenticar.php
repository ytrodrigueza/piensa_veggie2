<?php
$correo = trim($_POST["correo"]," '") ;
$clave = $_POST["clave"];

//instancia recibe  correo y clave y eso necesito para el DAO
$administrador=new Administrador("","","",$correo,$clave,"");
$cliente=new Cliente("","","",$correo,$clave,"","");


if($administrador->autenticar()){
    
    //si se autentica el admi entonces se crea una variable de sesion que tendrá el id del admi
    $_SESSION["id"]=$administrador->getId();
    
    //y se crea variable de sesion con un rol que llamo administrador, porque existen varios usuarios
    $_SESSION["rol"]="administrador";
    
    //necesito redireccionar a la sesion de admi para que me muestre los servicios para el admi
    header("Location: index.php?pid=".base64_encode("presentacion/sesion_admi.php"));
    
    echo "se autentica admi <br>" ;
    echo $administrador->getId();

}else if($cliente->autenticar()){
    //si se autentica el cliente entonces se crea una variable de sesion que tendrá el id del cliente
    // y arrojará falso el metodo autenticar en administrador
    $_SESSION["id"]=$cliente->getId();
    
    //y se crea variable de sesion con un rol que llamo cliente, porque existen varios usuarios
    $_SESSION["rol"]="cliente";
    
    //necesito redireccionar a la sesion de cliente para que me muestre los servicios para el cliente
    header("Location: index.php?pid=".base64_encode("presentacion/sesion_cliente.php"));
    
    echo "se autentica cliente <br>" ;
    echo $cliente->getId();
    
}else {
    $domiciliario= new Domiciliario("","","",$correo,$clave,"","");
    if($domiciliario->autenticar()){
        
        $_SESSION["id"]=$domiciliario->getId();
        //y se crea variable de sesion con un rol que llamo domiciliario, porque existen varios usuarios
        $_SESSION["rol"]="domiciliario";
        
        //necesito redireccionar a la sesion de admi para que me muestre los servicios para el admi
        header("Location: index.php?pid=".base64_encode("presentacion/sesion_domiciliario.php"));
        
        echo "se autentica domiciliario <br>" ;
        echo $domiciliario->getId();
       
    }else{
        echo "no se autentica";
    }
 
}

?>