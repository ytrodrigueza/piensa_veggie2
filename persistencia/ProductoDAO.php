<?php

class ProductoDAO{
    private $id;
    private $descripcion;
    private $precio;
    private $inventario;
    private $administrador;
    private $categoria;
   
    
    public function ProductoDAO($id, $descripcion, $precio,  $inventario, $administrador,$categoria){
        $this -> id = $id;
        $this -> descripcion = $descripcion;
        $this -> precio = $precio;
        $this -> inventario = $inventario;
        $this -> administrador = $administrador;
        $this -> categoria = $categoria;
      
    }
    

    public function registrar(){
        return "insert into producto (id,descripcion, precio, inventario, fk_administrador, fk_categoria)
                values (
                '" . $this -> id . "',
                '" . $this -> descripcion . "',
                '" . $this -> precio . "',
                '" . $this -> inventario . "',
                '" . $this -> administrador . "',
                '" . $this -> categoria . "'
               
                )";
    }


    public function consultar_todo(){
        return "select id, descripcion, precio, inventario, fk_administrador, fk_categoria
                from producto
                order by descripcion asc";
    }
    
    public function consultar(){
        return "select id, descripcion, precio, inventario, fk_administrador, fk_categoria
                from producto
                 where id='".$this->id."'";
    }
   
    
    public function actualizar_inventario(){
        return "update producto 
                set inventario='".$this->inventario."'
                where id='".$this->id."'"; 
    }


    public function productos_por_categoria(){
       
        return "select c.descripcion as Categoria, count(p.id) as Cantidad
                from categoria c left join producto p on (c.id = p.fk_categoria)
                group by c.descripcion
                order by c.descripcion asc";
    }
    
    public function productos_por_fecha(){
        
        return "select f.fecha as Fecha, SUM(ped.cantidad) as Cantidad
                from factura f inner join pedido ped on  (f.id=ped.fk_factura)
                group by f.fecha 
                order by f.fecha asc";
    }
   
    
    
}


?>