<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/FacturaDAO.php";


class Factura{
    
    private $id;
    private $fecha;
    private $hora;
    private $subtotal;
    private $impuesto;
    private $total;
    private $cliente;
    private $domiciliario;
    private $metodo_pago;
    private $conexion;
    private $facturaDAO;
    
   
    
    public function getId()
    {
        return $this->id;
    }

   
    public function getFecha()
    {
        return $this->fecha;
    }

   
    public function getHora()
    {
        return $this->hora;
    }

    
    public function getSubtotal()
    {
        return $this->subtotal;
    }

    public function getImpuesto()
    {
        return $this->impuesto;
    }

    public function getTotal()
    {
        return $this->total;
    }

   
    public function getCliente()
    {
        return $this->cliente;
    }

  
   
    public function getDomiciliario()
    {
        return $this->domiciliario;
    }

    
    public function getMetodo_pago()
    {
        return $this->metodo_pago;
    }

    
     
    
    public function Factura($id=0, $fecha="", $hora="",$subtotal="",  $impuesto="", $total="",$cliente="",$domiciliario="",$metodo_pago=""){
        $this -> id = $id;
        $this -> fecha = $fecha;
        $this -> hora = $hora;
        $this->subtotal=$subtotal;
        $this -> impuesto = $impuesto;
        $this -> total = $total;
        $this -> cliente = $cliente;     
        $this -> domiciliario=$domiciliario;
        $this -> metodo_pago = $metodo_pago;
        $this -> conexion = new Conexion();
        $this -> facturaDAO = new FacturaDAO($this->id, $this->fecha,  $this->hora, $this->subtotal, $this->impuesto, $this->total,$this->cliente,$this->domiciliario,$this->metodo_pago );
    }
    
    
    //consultar todas las facturas puede ser servicio para el admi
    public function consultar(){
        $this -> conexion -> abrir();
        $this->conexion->ejecutar($this->facturaDAO->consultar());
        $resultado= $this->conexion->extraer();
        $this -> conexion -> cerrar();
        $this->id= $resultado[0];
        $this -> fecha = $resultado[1];
        $this -> hora = $resultado[2];
        $this->subtotal=$resultado[3];
        $this -> impuesto = $resultado[4];
        $this -> total = $resultado[5];
        $cliente=new Cliente($resultado[6]);
        $cliente->consultar();
        $this->cliente=$cliente;
        $domiciliario=new Domiciliario($resultado[7]);
        $domiciliario->consultar();
        $this->domiciliario=$domiciliario;
        $metodo_pago=new Metodo_pago($resultado[8]);
        $metodo_pago->consultar();
        $this->metodo_pago=$metodo_pago;
        
        
    }
    
    public function crear_factura(){
        
        $this -> conexion -> abrir();
        $this->conexion->ejecutar($this->facturaDAO->crear_factura());
        $this -> conexion -> cerrar();
        
    }
    
    public function consultar_ultimo_ingreso(){
        
        $this -> conexion -> abrir();
        $this->conexion->ejecutar($this->facturaDAO->consultar_ultimo_ingreso());
        $resultado= $this->conexion->extraer();
        $this -> conexion -> cerrar();
        return $resultado[0];
        
    }
    
   
    
    
    
}

?>