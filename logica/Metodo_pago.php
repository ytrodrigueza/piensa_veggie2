<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/Metodo_pagoDAO.php";

class Metodo_pago{
    
    private $id;
    private $descripcion;
    private $conexion;
    private $metodo_pagoDAO;
    
    
    
    
    /**
     * @return number
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

   
    public function Metodo_pago($id=0, $descripcion=""){
        $this -> id = $id;
        $this -> descripcion = $descripcion;
        $this -> conexion = new Conexion();
        $this -> metodo_pagoDAO = new Metodo_pagoDAO($id, $descripcion);
    }
    
    
    public function consultar(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->metodo_pagoDAO->consultar());
        $datos= $this->conexion->extraer();
        $this->conexion->cerrar();
        $this->id =$datos[0];
        $this->descripcion =$datos[1];
     
        
    }
    
    
    public function consultar_todos(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->metodo_pagoDAO->consultar_todos());
        $metodos_p=array();
        while(($resultado=$this->conexion->extraer())!= null) {
            $metodo_p= new Metodo_pago($resultado[0],$resultado[1]);
            array_push($metodos_p,$metodo_p);
            
        }
        $this->conexion->cerrar();
        return $metodos_p;
        
    }
    
    
}