<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/CategoriaDAO.php";

class Categoria{
    private $id;
    private $descripcion;
    private $conexion;
    private $categoriaDAO;
    
    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }
    
    public function Categoria ($id=0, $descripcion=""){
        $this -> id = $id;
        $this -> descripcion = $descripcion;
        $this -> conexion = new Conexion();
        $this -> categoriaDAO = new CategoriaDAO($id, $descripcion);
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> categoriaDAO -> consultar());
        $resultado = $this -> conexion -> extraer();
        $this -> descripcion = $resultado[0];
        $this -> conexion -> cerrar();
    }
    
    
    
    public function consultar_todo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> categoriaDAO -> consultar_todo());
        $categorias = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($categorias, new Categoria($resultado[0], $resultado[1]));
        }
        $this -> conexion -> cerrar();
        return $categorias;
    }
    
}