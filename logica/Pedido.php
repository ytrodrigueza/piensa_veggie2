<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/PedidoDAO.php";

class Pedido{
    
    
    private $cantidad;
    private $subtotal;
    private $producto;
    private $factura;
    private $conexion;
    private $pedidoDAO;
   
    public function getCantidad()
    {
        return $this->cantidad;
    }

   
    public function getSubtotal()
    {
        return $this->subtotal;
    }

   
    public function getProducto()
    {
        return $this->producto;
    }

    
    public function getFactura()
    {
        return $this->factura;
    }

  

    public function Pedido($cantidad="", $subtotal="", $producto="",  $factura=""){
        $this -> cantidad = $cantidad;
        $this -> subtotal = $subtotal;
        $this -> producto = $producto;
        $this -> factura = $factura;
        $this -> conexion = new Conexion();
        $this -> pedidoDAO = new PedidoDAO($cantidad, $subtotal,  $producto,  $factura );
    }
    
    public function consultar_por_factura(){
        $this -> conexion -> abrir();
        echo $this->pedidoDAO->consultar_por_factura();
        echo $this->factura;
        $this->conexion->ejecutar($this->pedidoDAO->consultar_por_factura());
        $mercado=array();
        
        //$pedido= $this->conexion->extraer() mientras eso sea verdad
        while(($resultado= $this->conexion->extraer())!= null){
            $producto=new Producto($resultado[2]);
           $producto->consultar();
           $this->producto=$producto;
           $factura=new Factura($resultado[3]);
           $factura->consultar();
           $this->factura= $factura;
           $pedido = new Pedido($resultado[0],$resultado[1],$producto,$factura);
           array_push($mercado, $pedido); 
        }
         $this -> conexion -> cerrar();
        return $mercado;
        
        
    }
    
 
   
    public function crear_pedido(){
        $this -> conexion -> abrir();
        $this->conexion->ejecutar($this->pedidoDAO->crear_pedido());
        $this -> conexion -> cerrar();
     
    }
    
}

?>