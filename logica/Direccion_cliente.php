<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/Direccion_clienteDAO.php";


class Direccion_cliente{
    private $id;
    private $descripcion;
    private $cliente;
    private $conexion;
    private $direccion_clienteDAO;
   
    

    
    
    /**
     * @return number
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    public function getCliente()
    {
        return $this->cliente;
    }
    
 



    public function Direccion_cliente($id=0, $descripcion="",$cliente=""){
        $this -> id = $id;
        $this -> descripcion = $descripcion;
        $this -> cliente = $cliente;
        $this -> conexion = new Conexion();
        $this -> direccion_clienteDAO = new Direccion_clienteDAO($this->id, $this->descripcion,$this->cliente);
    }
    
    
    
    public function consultar(){
        
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->direccion_clienteDAO->consultar());
        $datos= $this->conexion->extraer();
        
        //posicion 0 porque esa fue la consulta que hice en el DAO
        //no estoy consultando el id, por eso es 0
        $this->descripcion =$datos[0];
        $this->conexion->cerrar();
        
    }
}
    
    ?>