<?php

require_once "persistencia/Conexion.php";
require_once "persistencia/ProductoDAO.php";

class Producto{
    private $id;
    private $descripcion;
    private $precio;
    private $inventario;
    private $administrador;
    private $categoria;
    private $conexion;
    private $productoDAO;
    
       
    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @return mixed
     */
    public function getInventario()
    {
        return $this->inventario;
    }

    
    public function getPrecio()
    {
        return $this->precio;
    }

   
    public function getCategoria()
    {
        return $this->categoria;
    }

   
    public function getAdministrador()
    {
        return $this->administrador;
    }

   

    public function Producto($id=0, $descripcion="", $precio="",  $inventario="", $administrador="",$categoria=""){
        $this -> id = $id;
        $this -> descripcion = $descripcion;
        $this -> precio = $precio;
        $this -> inventario = $inventario;
        $this -> administrador = $administrador;
        $this -> categoria = $categoria;
        $this -> conexion = new Conexion();
        $this -> productoDAO = new ProductoDAO($this->id, $this->descripcion,  $this->precio,  $this->inventario, $this->administrador,$this->categoria );
    }
    
    public function registrar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO ->registrar());
        $this -> conexion -> cerrar();
    }
    
    
    public function consultar_todo(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> consultar_todo());
        $productos=array();
    
        //select id, descripcion, precio, inventario, fk_administrador, fk_categoria
        while(($resultado=$this->conexion->extraer())!= null) {
            $administrador = new Administrador($resultado[4]);
            $administrador -> consultar();
            $categoria = new Categoria($resultado[5]);
            $categoria -> consultar();
            $producto= new Producto($resultado[0],$resultado[1],$resultado[2],$resultado[3],$administrador,$categoria);
            array_push($productos,$producto);
            
        }
        
        return $productos;
        
        
    }
    
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> consultar());
        $resultado = $this -> conexion -> extraer();
        $administrador = new Administrador($resultado[4]);
        $administrador -> consultar();
        $categoria = new Categoria($resultado[5]);
        $categoria -> consultar();
        $this -> id = $resultado[0];
        $this -> descripcion = $resultado[1];
        $this -> precio = $resultado[2];
        $this -> inventario = $resultado[3];
        $this -> administrador = $administrador;
        $this -> categoria = $categoria;
        $this->conexion->cerrar();
        
    }
    
    
    public function actualizar_inventario(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> actualizar_inventario());
        $this -> conexion -> cerrar();
        
    }
    
    
    public function productos_por_categoria(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> productos_por_categoria());
        $categorias = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($categorias, $resultado);
        }
        $this -> conexion -> cerrar();
        return $categorias;
    }
    
    public function productos_por_fecha(){
        
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> productos_por_fecha());
        $fechas = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($fechas, $resultado);
        }
        $this -> conexion -> cerrar();
        return $fechas;
        
    }
    
    
    
    
    
    
    
    public function consultarTodos($atributo, $direccion, $filas, $pag){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> consultarTodos($atributo, $direccion, $filas, $pag));
        $articulos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $administrador = new Administrador($resultado[7]);
            $administrador -> consultar();
            $categoria = new Categoria($resultado[6]);
            $categoria -> consultar();
            array_push($articulos, new Producto($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4],  $marca, $categoria,$administrador));
                  
        
        }
        $this -> conexion -> cerrar();
        return $articulos;
    }
    
    

    
    
    
    
    
    public function consultarTodosReporte(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> consultarTodosReporte());
        $articulos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $administrador = new Administrador($resultado[7]);
            $administrador -> consultar();
            $categoria = new Categoria($resultado[6]);
            $categoria -> consultar();
            array_push($articulos, new Producto($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4],  $marca, $categoria,$administrador));
        }
        $this -> conexion -> cerrar();
        return $articulos;
    }
    
    public function consultarTotalFilas(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> consultarTotalFilas());
        return $this -> conexion -> extraer()[0];
    }
    
    public function consultarFiltro($filtro){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> consultarFiltro($filtro));
        $articulos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $administrador = new Administrador($resultado[7]);
            $administrador -> consultar();
            $categoria = new Categoria($resultado[6]);
            $categoria -> consultar();
            array_push($articulos, new Producto($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4],  $marca, $categoria,$administrador));
        }
        $this -> conexion -> cerrar();
        return $articulos;
    }
    
    
    
   
    
    public function consultarProductosPorCategoria(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO -> consultarProductosPorCategoria());
        $articulos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            array_push($articulos, $resultado);
        }
        $this -> conexion -> cerrar();
        return $articulos;
    }
    
    public function consultarArticulosCategoria($id){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> productoDAO ->consultarArticulosCategoria($id));
        $articulos = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $categoria = new Categoria($resultado[6]);
            $categoria -> consultar();
            array_push($articulos, new Producto($resultado[0], $resultado[1], $resultado[2], $resultado[3], $resultado[4],  $marca, $categoria));
        }
        $this -> conexion -> cerrar();
        return $articulos;
        
     }
        
      
    }
    
    
    
    
    





























?>