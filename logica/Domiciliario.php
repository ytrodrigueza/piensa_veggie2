<?php
require_once 'persistencia/Conexion.php';
require_once 'persistencia/DomiciliarioDAO.php';

class Domiciliario{
    
    private $id;
    private $nombre;
    private $apellido;
    private $correo;
    private $clave;
    private $celular;
    private $estado;
    private $conexion;
    private $domiciliarioDAO;
    
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * @return mixed
     */
    public function getNombre()
    {
        return $this->nombre;
    }
    
    /**
     * @return mixed
     */
    public function getApellido()
    {
        return $this->apellido;
    }
    
    /**
     * @return mixed
     */
    public function getCorreo()
    {
        return $this->correo;
    }
    
    /**
     * @return mixed
     */
    public function getClave()
    {
        return $this->clave;
    }
    
    /**
     * @return mixed
     */
    public function getCelular()
    {
        return $this->celular;
    }
    
    /**
     * @return mixed
     */
    public function getEstado()
    {
        return $this->estado;
    }
    

  
   
    
    public function Domiciliario($id=0,$nombre="",$apellido="",$correo="",$clave="",$celular="",$estado=""){
        
        $this->id =$id;
        $this->nombre =$nombre;
        $this->apellido =$apellido;
        $this->correo =$correo;
        $this->clave =$clave;
        $this->celular =$celular;
        $this->estado =$estado;
        $this->conexion= new Conexion();
        $this->domiciliarioDAO=new DomiciliarioDAO( $this->id, $this->nombre, $this->apellido, $this->correo,  $this->clave,  $this->celular,  $this->estado);
        
    }
    
    public function autenticar(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->domiciliarioDAO->autenticar());
        $this->conexion->cerrar();
        
        if($this -> conexion -> numFilas() == 0){
            return false;
        }else{
            $resultado = $this -> conexion -> extraer();
            $this -> id = $resultado[0];
            return true;
        }
     
    }
    
    public function consultar(){
        $this->conexion->abrir();
        $this->conexion->ejecutar($this->domiciliarioDAO->consultar());
        $datos= $this->conexion->extraer();
        $this->conexion->cerrar();
        //posicion 0 porque esa fue la consulta que hice en el DAO
        //no estoy consultando el id, por eso es 0
        $this->id=$datos[0];
        $this->nombre =$datos[1];
        $this->apellido =$datos[2];
        $this->correo =$datos[3];
       
        
    }
    
}
